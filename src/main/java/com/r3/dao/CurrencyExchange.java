package com.r3.dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import org.junit.Assert;
import com.r3.utils.RequestsUtil;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class CurrencyExchange extends RequestsUtil {
	public static final String directoryLoc = System.getProperty("user.dir");
    private static final String SCHEMA_FILE_PATH = directoryLoc+"/src/test/resources/testData/schema.json";
	Properties prop = new Properties();
	static RequestSpecification request;
	public static Response response;

	public CurrencyExchange() throws IOException {
		super();
	}

	public void sendGetRequest(String country, String apiName) throws IOException {
		setApiURI(apiName, country);
        getRequest(getApiURI());
	}

	public void isAPICallSuccessfull() {
		Assert.assertNotNull(RequestsUtil.response);
		 System.out.println("API call is successful.");
	}

	public void checkStatusCodeToBe(String statuscode) {
		Assert.assertEquals(RequestsUtil.response.getStatusCode(), Integer.parseInt(statuscode));

	}

	public void checkStatusReturned(String status) {
		JSONObject jsonResponse = new JSONObject(RequestsUtil.response.getBody().asString());
	    String actualStatus = jsonResponse.getString("result");
	    Assert.assertEquals(status.toLowerCase(), actualStatus);
	    System.out.println("Status code matches the expected value: " + status);

	}

	public void checkRatesRange(String currency, String fromRange, String toRange) {		
		JSONObject jsonResponse = new JSONObject(RequestsUtil.response.getBody().asString());
        JSONObject rates = jsonResponse.getJSONObject("rates");
        double currencyPrice = rates.getDouble(currency);
        Assert.assertTrue(currency + " price should be between " + fromRange + " and " + toRange,
                currencyPrice >= Double.parseDouble(fromRange) && currencyPrice <= Double.parseDouble(toRange));
        System.out.println(currency + " price is within the range: " + fromRange + " - " + toRange);

	}

	public void verifyNumberOfCurrencyPairs(String expectedNumberOfPairs) {
        JSONObject jsonResponse = new JSONObject(RequestsUtil.response.getBody().asString());
        JSONObject rates = jsonResponse.getJSONObject("rates");
        int numberOfPairs = rates.length();
        Assert.assertEquals("Number of currency pairs", Integer.parseInt(expectedNumberOfPairs), numberOfPairs);
        System.out.println("Number of currency pairs matches the expected value: " + expectedNumberOfPairs);
    
	}


	public void testResponseJsonSchema() throws IOException {
        JSONObject schemaObject = new JSONObject(new JSONTokener(readFileContent(SCHEMA_FILE_PATH)));
        JSONObject responseJson = new JSONObject(RequestsUtil.response.getBody().asString());
        Schema schema = SchemaLoader.load(schemaObject);
        schema.validate(responseJson);
        System.out.println("Schema validation passed successfully.");
    }

	
    private String readFileContent(String filePath) throws IOException {
        return new String(Files.readAllBytes(Paths.get(filePath)));
    }

}
