package com.r3.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.annotations.Step;

public class RequestsUtil {
	public static String jsonBody;
	static RequestSpecification request;
	public static Response response;
	private String apiUri;
	public static final String DIRECTORY_LOC = System.getProperty("user.dir");
	private static final String API_PROPERTIES_PATH = DIRECTORY_LOC + "/src/test/resources/testData/api.properties";

	Properties prop = new Properties();

	protected RequestsUtil() throws IOException {
		setBaseURI();
		request = RestAssured.given();
	}

	public void setBaseURI() throws IOException {
		try (FileInputStream apiFile = new FileInputStream(API_PROPERTIES_PATH)) {
			prop.load(apiFile);
			RestAssured.baseURI = prop.getProperty("baseURI");
		}
	}

	public String getBaseURI() {
		return RestAssured.baseURI;
	}

	public void setApiURI(String apiName, String countryName) throws IOException {
		try (FileInputStream apiFile = new FileInputStream(API_PROPERTIES_PATH)) {
			prop.load(apiFile);
			this.apiUri = prop.getProperty(apiName).replace("{currency}", countryName);
		}
	}

	public String getApiURI() {
		return apiUri;
	}

	@Step("API details")
	public void getRequest(String apiURI) throws IOException {
		response = request.get(apiURI);
	}

	@Step("API details")
	public void postRequest(String apiURI) throws IOException {
		// can add post logic for future reference
	}
}
