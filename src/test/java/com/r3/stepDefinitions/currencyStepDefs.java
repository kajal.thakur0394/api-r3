package com.r3.stepDefinitions;

import java.io.IOException;

import com.r3.dao.CurrencyExchange;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class currencyStepDefs {

	CurrencyExchange currency;

    public currencyStepDefs() throws IOException {
        currency = new CurrencyExchange();
    }

	
	@Given("I send a GET request to the {string} currency exchange for {string} API")
	public void i_send_a_get_request_to_the_currency_exchange_for_api(String country, String apiName) throws IOException {
		currency.sendGetRequest(country,apiName);
	}

	@Then("the API call should be successful")
	public void the_api_call_should_be_successful() {
		currency.isAPICallSuccessfull();
	}

	@Then("the status code should be {string}")
	public void the_status_code_should_be(String statuscode) {
		currency.checkStatusCodeToBe(statuscode);
	}

	@Then("the status returned by the API should be {string}")
	public void the_status_returned_by_the_api_should_be(String status) {
		currency.checkStatusReturned(status);
	}

	@Then("the USD price against {string} should be within range of {string} to {string}")
	public void the_usd_price_against_should_be_within_range_of_to(String currencyRate, String fromRange, String toRange) {
		currency.checkRatesRange(currencyRate,fromRange,toRange);
		
	}

	@Then("the API should return {string} currency pairs")
	public void the_api_should_return_currency_pairs(String count) {
		currency.verifyNumberOfCurrencyPairs(count);
	}

	@Then("the API response should match the JSON schema")
	public void the_api_response_should_match_the_json_schema() throws IOException {
		currency.testResponseJsonSchema();
	}

}
