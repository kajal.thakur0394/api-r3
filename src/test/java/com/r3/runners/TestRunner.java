package com.r3.runners;
import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/test/resources/features/" },
		glue = { "com.r3.stepDefinitions" }, 
		plugin = {
				"pretty", "json:target/cucumber-reports/Cucumber.json", "junit:target/cucumber-reports/Cucumber.xml", },

		tags = "@currency",

		monochrome = true)

public class TestRunner {
	
}
