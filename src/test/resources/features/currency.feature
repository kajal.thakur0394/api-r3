@currency
Feature: Validate API response for currency exchange rates

  Scenario: Verify successful API response
    Given I send a GET request to the "USD" currency exchange for "currencyDetails" API
    Then the API call should be successful
    And the status code should be "200"
    And the status returned by the API should be "SUCCESS"

  Scenario: Verify USD price against AED is in range
    Given I send a GET request to the "USD" currency exchange for "currencyDetails" API
    Then the USD price against "AED" should be within range of "3.6" to "3.7"

  Scenario: Verify the number of currency pairs returned
    Given I send a GET request to the "USD" currency exchange for "currencyDetails" API
    Then the API should return "162" currency pairs

  Scenario: Verify API response matches JSON schema
    Given I send a GET request to the "USD" currency exchange for "currencyDetails" API
    Then the API response should match the JSON schema
