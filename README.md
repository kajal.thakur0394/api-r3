# api-r3


# Requirements
In order to setup this project you'll need following things installed and configured on your local machine
* Maven
* Java

* Java IDE
    * Cucumber Plugin
# Tech Stack
* Cucumber
* RestAssured API
* JUnit
# Usage
To execute this as a maven project:
* mvn clean compiler:compile
* mvn clean verify

To execute this from TestRunner.java:
* Right click->Run as->Junit from TestRunner.java 

# Report
Basic cucumber report can be found in the following path: 
{projectDirectory}/target/cucumber-reports/index.html